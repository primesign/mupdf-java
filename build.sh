#!/bin/bash
set -eu
set -o pipefail

curl -fLs ${MUPDF_SOURCE} --output download.tar.gz
echo "${MUPDF_CHECKSUM} download.tar.gz" | sha1sum --check
tar -xf download.tar.gz
VERSION=`ls|grep -E "mupdf-.+-source"|cut -f 2 -d "-"`
SRC=mupdf-$VERSION-source
mkdir -p dist
cp CMakeLists.txt $SRC
cmake3 -DCMAKE_BUILD_TYPE=Release -DJAVA_HOME=${JAVA_HOME} -S ${SRC} -B dist
cmake3 --build dist